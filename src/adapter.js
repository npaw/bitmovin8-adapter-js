var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Bitmovin8 = youbora.Adapter.extend({

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = this.player ? this.player.getCurrentTime() : null
    if (this.plugin.getAdsAdapter() && this.plugin.getAdsAdapter().flags.isStarted) {
      ret = this.lastPlayhead || 0
    }
    return ret
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    return this.player ? this.player.getPlaybackSpeed() : null
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return this.player ? this.player.getDroppedVideoFrames() : null
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = null
    if (this.player) {
      this.lastDuration = this.player.getDuration() || this.lastDuration
      ret = this.lastDuration
    }
    return ret
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var ret = null
    if (this.player) {
      var videodata = this.player.getPlaybackVideoData()
      var videoquality = this.player.getVideoQuality()
      if (videoquality && videoquality.bitrate) {
        ret = videoquality.bitrate
      } else if (videodata && videodata.bitrate) {
        ret = videodata.bitrate
      }
    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    var ret = null
    if (this.player) {
      var videodata = this.player.getPlaybackVideoData()
      var videoquality = this.player.getVideoQuality()
      if (videoquality && videoquality.bitrate) {
        ret = youbora.Util.buildRenditionString(videoquality.width, videoquality.height, videoquality.bitrate)
      } else if (videodata && videodata.bitrate) {
        ret = youbora.Util.buildRenditionString(videodata.width, videodata.height, videodata.bitrate)
      }
    }
    return ret
  },

  /** Override to return title */
  getTitle: function () {
    return this.player ? this.player.getSource().title : null
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.player ? this.player.isLive() : null
  },

  /** Override to return resource URL. */
  getResource: function () {
    var ret = null
    if (this.player && this.player.getSource && this.player.getSource()) {
      try {
        if (this.player.getStreamType && this.player.getStreamType()) {
          if (this.player.getSource()[this.player.getStreamType()]) {
            return this.player.getSource()[this.player.getStreamType()]
          }
        }
      } catch (e) {}
      ret = this.player.getSource().progressive || this.player.getSource().hls
    }
    try {
      if (this.player && this.player.getManifest && this.player.getManifest()) {
        if (this.player.getManifest().indexOf('MPD') > 0) {
          ret = this.player.getSource().dash
        } else {
          ret = this.player.getSource().hls
        }
      }
    } catch (err) {
      youbora.Log.notice('Cant get manifest')
    }
    return ret
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player ? this.player.version : null
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Bitmovin'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    var events = this.player.exports.PlayerEvent

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    // References
    this.references = {}
    this.references[events.Play] = this.playListener.bind(this)
    this.references[events.Paused] = this.pauseListener.bind(this)
    this.references[events.Playing] = this.playingListener.bind(this)
    this.references[events.Error] = this.errorListener.bind(this)
    this.references[events.Seek] = this.seekingListener.bind(this)
    this.references[events.TimeShift] = this.seekingListener.bind(this)
    this.references[events.TimeShifted] = this.seekedListener.bind(this)
    this.references[events.PlaybackFinished] = this.endedListener.bind(this)
    this.references[events.TimeChanged] = this.timeupdateListener.bind(this)
    this.references[events.SourceUnloaded] = this.endedListener.bind(this)
    this.references[events.Destroy] = this.destroyListener.bind(this)
    this.references[events.Ready] = this.readyListener.bind(this)
    this.references[events.AdBreakStarted] = this._checkAdsAdapter.bind(this)
    // Register listeners
    for (var key in this.references) {
      this.player.on(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    try {
      if (this.plugin){
        this.plugin.removeAdsAdapter()
      }
      if (this.player && this.player.off && this.references) {
        for (var key in this.references) {
          this.player.off(key, this.references[key])
        }
        delete this.references
      }
    } catch (err) {
      // player was destroyed previously, nothing
    }
  },

  _checkAdsAdapter: function () {
    var adsAdapterInst = this.plugin.getAdsAdapter()
    if (!adsAdapterInst) {
      this.plugin.setAdsAdapter(new youbora.adapters.Bitmovin8.AdsAdapter(this.player))
    } else if (adsAdapterInst && !adsAdapterInst.player) {
      adsAdapterInst.setPlayer(this.player)
    }
  },

  /** Listener for 'ready' event. */
  readyListener: function (e) {
    this._checkAdsAdapter()
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.lastPlayhead = 0
    this.fireStart()
    this.failedView = false
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function (e) {
    if (!this.plugin.getAdsAdapter() || !this.plugin.getAdsAdapter().flags.isStarted) {
      this.lastPlayhead = this.player.getCurrentTime()
    }
    if (this.getPlayhead() > 0.2 &&
      (this.getIsLive() || this.getPlayhead() + 1 < this.getDuration())) {
      this.fireStart()
      this.failedView = false
      if (!this.flags.isJoined) {
        this.monitor.skipNextTick()
        this.fireJoin()
      }
    }
    if (!this.player.isPaused() && this.flags.isSeeking) {
      this.fireSeekEnd()
      this.monitor.skipNextTick()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    if (e.issuer && e.issuer === 'ui-seek') {
      this.fireSeekBegin()
    } else {
      this.firePause()
    }
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireStart()
    this.failedView = false
    this.fireResume()
    if (!this.flags.isJoined) {
      this.monitor.skipNextTick()
      // this.fireJoin()
    }
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    if (this.failedView) return
    if (!this.flags.isStarted) this.failedView = true
    this.fireFatalError(e.code, e.name)
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    this.fireSeekEnd()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
  },

  destroyListener: function (e) {
    this.player = null
    this.tag = null
    this.fireStop()
    this.plugin.removeAdapter()
  }
},
// Static members
{
  AdsAdapter: require('./ads/adsAdapter')
}
)

module.exports = youbora.adapters.Bitmovin8
