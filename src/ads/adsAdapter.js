var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var AdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-Bitmovin8-ads'
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = null
    if (this.player) {
      ret = this.player.getCurrentTime()
    }
    return ret
  },

  getTitle: function () {
    return this.title
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.duration
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.src
  },

  /** Override to return rendition */
  getRendition: function () {
    return youbora.Util.buildRenditionString(this.width, this.height, this.bitrate)
  },

  getBitrate: function () {
    return this.bitrate * 1000
  },

  // getGivenBreaks: function () { },
  // getBreaksTime: function () { },
  // getGivenAds: function () { },

  getAudioEnabled: function () {
    var ret = true
    if (this.player) {
      ret = this.player.getVolume() > 0 && !this.player.isMuted()
    }
    return ret
  },

  getIsSkippable: function () {
    return this.skippable
  },

  getIsFullscreen: function () {
    var ret = false
    if (this.player) {
      ret = this.player.getViewMode() === 'fullscreen'
    }
    return ret
  },

  getIsVisible: function () {
    var ret = false
    if (this.player && this.player.getVideoElement && this.player.getVideoElement()) {
      ret = youbora.Util.calculateAdViewability(this.player.getVideoElement())
    }
    return ret
  },

  getCreativeId: function () {
    return this.creativeId
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    var ret = this.position
    var adapterInst = this.plugin.getAdapter()
    if (adapterInst && !adapterInst.flags.isJoined){
      ret = youbora.Constants.AdPosition.Preroll
    }
    return ret
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    var events = this.player.exports.PlayerEvent

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)

    // References
    this.references = {}
    this.references[events.AdBreakStarted] = this.breakStartListener.bind(this)
    this.references[events.AdBreakFinished] = this.breakEndListener.bind(this)
    this.references[events.AdStarted] = this.adStartListener.bind(this)
    this.references[events.AdFinished] = this.adEndListener.bind(this)
    this.references[events.AdQuartile] = this.quartileListener.bind(this)
    this.references[events.AdSkipped] = this.skipListener.bind(this)
    this.references[events.AdClicked] = this.clickListener.bind(this)
    this.references[events.AdError] = this.errorListener.bind(this)
    this.references[events.Destroy] = this.destroyListener.bind(this)
    this.references[events.Paused] = this.pauseListener.bind(this)
    this.references[events.Playing] = this.resumeListener.bind(this)

    // Register listeners
    for (var key in this.references) {
      this.player.on(key, this.references[key])
    }
  },

  pauseListener: function () {
    this.firePause()
  },

  resumeListener: function() {
    this.fireResume()
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()
    // unregister listeners
    try {
      if (this.player && this.player.off && this.references) {
        for (var key in this.references) {
          this.player.off(key, this.references[key])
        }
        delete this.references
      }
    } catch (err) {
      // player was destroyed previously, nothing
    }
  },

  breakStartListener: function (e) {
    this.plugin.fireInit()
    var adapterInst = this.plugin.getAdapter()
    if (adapterInst) {
      adapterInst.firePause()
    }
    var position = e.adBreak.position
    if (position === 'pre') {
      this.position = youbora.Constants.AdPosition.Preroll
    } else if (adapterInst && adapterInst.getDuration() !== 0 && (adapterInst.getPlayhead() || position) > adapterInst.getDuration() - 1) {
      this.position = youbora.Constants.AdPosition.Postroll
    } else {
      this.position = youbora.Constants.AdPosition.Midroll
    }
  },

  breakEndListener: function (e) {
    this.fireBreakStop()
    if (this.plugin.requestBuilder.lastSent.adPosition !== youbora.Constants.AdPosition.Postroll) {
      var adapterInst = this.plugin.getAdapter()
      if (adapterInst) {
        adapterInst.fireResume()
      }
    } else {
      this.plugin.fireStop()
    }
  },

  adStartListener: function (e) {
    if (e.ad && e.ad.data) {
      this.title = e.ad.data.adTitle
      this.bitrate = e.ad.data.bitrate
      this.creativeId = (e.ad.data.creative) ? e.ad.data.creative.id : undefined
      this.src = e.ad.mediaFileUrl
      this.height = e.ad.height
      this.width = e.ad.width
      this.skippable = e.ad.skippable
      this.duration = e.ad.duration
    }
    this.counter = 0
    this.fireStart()
    this.fireJoin()
  },

  adEndListener: function (e) {
    this.fireStop()
    if (this.plugin.requestBuilder.lastSent.adPosition === youbora.Constants.AdPosition.Postroll) this.plugin.fireStop()
  },

  quartileListener: function (e) {
    var position = 1
    switch (e.quartile) {
      case 'midpoint':
        position = 2
        break
      case 'thirdQuartile':
        position = 3
    }
    this.fireQuartile(position)
  },

  skipListener: function (e) {
    this.fireSkip()
    if (this.plugin.requestBuilder.lastSent.adPosition === youbora.Constants.AdPosition.Postroll) this.plugin.fireStop()
  },

  clickListener: function (e) {
    this.fireClick(e.clickThroughUrl)
  },

  errorListener: function (e) {
    var errObj = e.data ? e.data : e
    this.fireError(errObj.code, errObj.message)
  },

  destroyListener: function (e) {
    this.player = null
    this.tag = null
    this.fireStop()
    this.plugin.removeAdsAdapter()
  }
})

module.exports = AdsAdapter
