## [6.8.4] - 2024-05-22
### Lib
- Packaged with lib version `6.8.56`

## [6.8.3] - 2023-08-09
### Fixed
- Use fireFatalError on errorListener, to allow conditioning the fatal error with the options object

## [6.8.2] - 2023-08-04
### Fixed
- On getResource method, check streamType if multiples sources are set, to get the defined streamType source

## [6.8.1] - 2023-02-03
### Added
- Fix issue when is parsing data object on adStartListener method

## [6.8.0] - 2022-02-23
### Added
- Listeners for timeshift events, reported as seek for live content
### Library
- Packaged with `lib 6.8.12`

## [6.7.7] - 2021-01-28
### Fixed
- Issue preventing ads to be reported when playing with multiple instances of the player/adapter

## [6.7.6] - 2020-09-10
### Library
- Packaged with `lib 6.7.17`

## [6.7.5] - 2020-09-03
### Fixed
- Fixed preroll detection
- AdPause/ AdResume events to be sent

## [6.7.4] - 2020-07-08
### Fixed
- Adapter and ads adapter automatically removed when player is destroyed

## [6.7.3] - 2020-07-01
### Fixed
- Checked player and adapter references in adsadapter to prevent errors when player is destroyed
### Library
- Packaged with `lib 6.7.11`

## [6.7.2] - 2020-06-25
### Library
- Packaged with `lib 6.7.10`

## [6.7.1] - 2020-04-16
### Added
- Try/catch to unregisterlisteners
### Library
- Packaged with `lib 6.7.5`

## [6.7.0] - 2020-04-09
### Library
- Packaged with `lib 6.7.2`

## [6.5.5] - 2020-01-30
### Library
- Packaged with `lib 6.5.25`

## [6.5.4] - 2019-11-12
### Fixed
 - Fake views after postrolls being created
### Library
- Packaged with `lib 6.5.19`

## [6.5.3] - 2019-11-04
### Fixed
 - Added src detection for hls on safari browsers
 - Postroll position detection
 - Closing views after postrolls
### Library
- Packaged with `lib 6.5.18`

## [6.5.2] - 2019-10-21
### Fixed
 - Workaround to detect ads when `ready` event is sent too late, too early or not sent at all
### Library
- Packaged with `lib 6.5.17`

## [6.5.1] - 2019-06-19
### Added
- Seek event for live
### Fixed
 - Fake buffers after seek removed
### Library
- Packaged with `lib 6.5.5`

## [6.5.0] - 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.4.4] - 2019-05-08
### Fixed
- Wrong bitrate when rendition is changed manually

## [6.4.3] - 2019-04-30
### Added
- Listeners for `unload` and `destroy` to close views
### Library
- Packaged with `lib 6.4.25`

## [6.4.2] - 2019-03-18
### Fixes
- Error without start being reported twice

## [6.4.1] - 2019-02-19
### Fixes
- Wrong player reference
### Library
- Packaged with `lib 6.4.16`

## [6.4.0] - 2018-11-20
### Library
- Packaged with `lib 6.4.9`
